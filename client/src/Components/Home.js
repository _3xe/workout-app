import './styles/home-style.scss';
import './styles/btn-styles.scss';

import { Link } from 'react-router-dom';
import { AiOutlineFire, AiOutlineCaretDown } from 'react-icons/ai';
import logo from '../img/logo.png';

const Home = () => {
  return (
    <div className='home-container'>
      <Link className='logo' to='/'><img src={ logo } alt="logo GYMINGUS" /></Link>
      <h1>Welcome!</h1>
      <div className="buttons">
        <Link to='/login'   className='login-btn home-btn'>Login</Link>
        <Link to='/register'className='signup-btn home-btn'>SignUp</Link>
      </div>
      <h2>Before you started, let's find out what is GYMINGUS!</h2>
      <AiOutlineCaretDown className='icon'/>
      <h3>With us you can:</h3>
      <ul>
        <li><AiOutlineFire className='fire-icon'/>Create your own training plan</li>
        <li><AiOutlineFire className='fire-icon'/>Write down the weight you use for each exercise</li>
        <li><AiOutlineFire className='fire-icon'/>Change exercise you don't like</li>
        <li><AiOutlineFire className='fire-icon'/>Start training timer</li>
        <li><AiOutlineFire className='fire-icon'/>Count breaks between exercises</li>
        <li><AiOutlineFire className='fire-icon'/>Count calories</li>
        <li><AiOutlineFire className='fire-icon'/>Use the calendar to track your workouts</li>
        <li><AiOutlineFire className='fire-icon'/>Join to our community! Share, post, make tips to others</li>
      </ul>
      <h2>Registration allows to use full advantage of our website, do not hesitate!</h2>
      <div className="buttons">
        <Link to='/create'  className='start-btn home-btn'>Start without account</Link>
      </div>
    </div>
  )
}

export default Home