import './styles/app-style.scss';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
// Components
import Navigation from './Navigation';
import Home from './Home';
import Create from './Create';
import Login from './Login';
import Register from './Register';
import Workout from './Workout';
import Calendar from './Calendar';
import Profile from './Profile';
import Error from './Error';

function App() {
  return (
  <Router>
    <div className='main-width'>
    <main>
      <Routes>
        <Route path='/'        element={<Home/>}    />
        <Route path='/login'   element={<Login/>}   />
        <Route path='/register'element={<Register/>}/>
        <Route path='/create'  element={<Create/>}  />
        <Route path='/workout' element={<Workout/>} />
        <Route path='/calendar'element={<Calendar/>}/>
        <Route path='/profile' element={<Profile/>} />
        <Route path='*'        element={ <Error/> } />
      </Routes>
    </main>
    </div>
    <Navigation/>
  </Router>
  );
}

// DO MEDIA FOR NAVIGATION
// DO TOOLTIPS FOR HOME UNORDERED LIST

export default App;
