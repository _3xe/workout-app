import './styles/navigation-style.scss';

import { MdCreate, MdFastfood } from 'react-icons/md';
import { CgGym, CgProfile } from 'react-icons/cg';
import { IoIosPeople} from 'react-icons/io';
import { AiTwotoneCalendar} from 'react-icons/ai';
import { Link } from 'react-router-dom';

import logo from '../img/logo.png';

const Navigation = () => {
  return (
      <ul className='navbar-list'>
        <li className='navbar-element'>
          <Link className='navbar-element__link'to='/workout'>
            <CgGym className='icon'/> <div className='tooltip'>Workout</div>
          </Link>
        </li>
        <li className='navbar-element'>
          <Link className='navbar-element__link'to='/calorie'>
            <MdFastfood className='icon' /> <div className='tooltip'>Calorie</div>
          </Link>
        </li>
        <li className='navbar-element'>
          <Link className='navbar-element__link'to='/calendar'>
            <AiTwotoneCalendar className='icon'/> <div className='tooltip'>Calendar</div>
          </Link>
        </li>

        <li className='navbar-element'>
          <Link className='navbar-element__link link-logo'to='/'>
            <img className='logo' src={ logo } alt="" /> <div className='tooltip'>Home</div>
          </Link>
        </li>
        
        <li className='navbar-element'>
          <Link className='navbar-element__link'to='/create'>
            <MdCreate className='icon' /> <div className='tooltip'>Create</div>
          </Link>
        </li>
        <li className='navbar-element'>
          <Link className='navbar-element__link'to='/community'>
            <IoIosPeople className='icon' /> <div className='tooltip'>Community</div>
          </Link>
        </li>
        <li className='navbar-element'>
          <Link className='navbar-element__link'to='/profile'>
            <CgProfile className='icon' /> <div className='tooltip'>Profile</div>
          </Link>
        </li>
      </ul>
  )
}

export default Navigation;