import './styles/error-style.scss';
import { Link } from 'react-router-dom';
import { AiOutlineArrowDown } from 'react-icons/ai';


const Error = () => {
  return (
    <div className='error-container'>
      <h1>ERROR 404</h1>
      <h2>OOPS! This site does not exist...</h2>
      <div className="error-container__return">
        
        <Link to='/' className='link'>
          <AiOutlineArrowDown class='icon icon-fir'/>
          Return to HOME PAGE
          <AiOutlineArrowDown class='icon icon-sec'/>
        </Link>
        
      </div>
    </div>
  )
}

export default Error